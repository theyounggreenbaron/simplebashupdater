#!/bin/bash

echo "======="
echo "Echo Checking for updates using APT"
echo "======="

sudo apt update && apt list --upgradable

echo "======="
echo "Upgrading available packages using APT"
echo "======="

sudo apt upgrade -y

echo "======="
echo "Updating flatpak packages"
echo "======="

flatpak update

echo -n "Press [ENTER] to continue... "
read var_name
echo "Goodbye!"
