#!/bin/bash

echo "====="
echo "====="
echo "Checking to see if you have apt updates, and what"
echo "====="
echo "====="

sudo apt update && apt list --upgradable

echo -n "Press [ENTER] to continue... "
read var_name
echo "Goodbye!"
